<?php
namespace warden\src;
require_once __DIR__ . '/DAO.php';
require_once __DIR__ . '/IdeaCategory.php';
use PDO;
use Exception;
use warden\src\DAO;
use warden\src\IdeaCategory;

class MysqlDAO implements DAO{
    
    /**
     * @var \PDO The PDO object connection
     */
    private $pdo;
    
    /**
     * @var string The table name
     */
    private $table;
    
    /**
     * @var mixed Array idea category according to \warden\src\IdeaCategory
     */
    private $category;

    /**
     * @var array A map from table field to Warden class attribute
     */
    private $map;
    
    /**
     * Create a Mysql Connection
     * @param string $database The database name
     * @param string $table The table name
     * @param mixed $category The idea category according to \warden\src\IdeaCategory
     * @param array $map Map table fields to Warden class attributes, eg:
     *                                  [ // Table_field     => WardenClassAttribute
     *                                       'mail_id'       =>  'id',
     *                                       'file_name'     =>  'name',
     *                                       'md5'           =>  'hash',
     *                                       'date_modified' =>  'detectTime'
     *                                   ]
     */
    public function __construct($database, $table, $category, $map){
        /**
         * @todo Set in development environment
         */
        $this->pdo = new PDO("mysql:host=localhost;dbname=$database",
                            '',
                            '');
        /**
         * @todo Set in production environment
        $this->pdo = new PDO("",
                            '',
                            '');
         */
        $this->table = $table;
        $this->category = $category;
        $this->map = $map;
    }

    /**
     * Retrieve unique Idea's from 'av_signatures' database table 'custom_signatures'
     * that were not already exported before
     * @param array $wardensIds Array of ID's already exported
     * @return array An array of Warden unique objects
     * @throws Exception
     */
    public function findByUniqueWardens($wardensIds){
        if (count($wardensIds) == 0)
            $wardensIds = array('all');
        if ($this->isConnected()){
            $placeHolders = implode(',', array_fill(0, count($wardensIds), '?'));
            $sqlStatement = $this->getBaseSQLStatement()
                            .' WHERE ' . array_search('id', $this->map)
                            ." NOT IN ($placeHolders)";
            return $this->getWardensFromPDO($sqlStatement, $wardensIds);
        }else{
            throw new Exception("Connection problems!", 40);
        }
    }
    
    /**
     * Retrieve new Wardens since last execution specified by $time parameter, eg: 
     *                  00:30:00 (Wardens whom 'date_modified' table field were 
     *                            updated 30 minutes ago will be retrieved)
     * @param string $time The time since last execution, use format: hh:mm:ss
     * @return array An array of Warden unique objects
     * @throws Exception
     */
    public function findByDetectTime($time){
        if (strtotime($time) !== FALSE){
            if ($this->isConnected()){
                $sqlStatement = $this->getBaseSQLStatement()
                                .' WHERE ' . array_search('detectTime', $this->map)
                                .' > ?';
                $timeParam = date(  'Y-m-d H:i:s', 
                                    time() - 7200 - strtotime('01/01/1970 '.$time));
                return $this->getWardensFromPDO($sqlStatement, array($timeParam));
            }else{
                throw new Exception("Connection problems!", 40);
            }
        }else{
            throw new Exception("This is not a valid time, use the format: hh:mm:ss", 41);
        }
    }
    
    /**
     * @return The initial base SQL
     */
    private function getBaseSQLStatement(){
            $fieldsAs = array_map( //CONVERT() is to make sure that all chars are ASCII
                            function($jsonField, $tableField){
                                return 'CONVERT('.$tableField.' USING ASCII) AS '.$jsonField;
                            },
                            $this->map, 
                            array_keys($this->map)
                        );
            return "SELECT " .implode(',', $fieldsAs)
                            ." FROM " .$this->table;
    }

    /**
     * Query the database and return Warden objects from it
     * @param $sqlStatement The SQL statement to be executed
     * @param array $params The SQL param for the statement
     * @return array Array of Warden objects
     */
    public function getWardensFromPDO($sqlStatement, $params=['']){
        if (is_array($params)){
            $query = $this->pdo->prepare($sqlStatement);
            //The result query will be transformed into Warden objects
            $query->setFetchMode(PDO::FETCH_CLASS, 'warden\src\Warden');
            //Parameter avoid query objects already exported, improving performance
            $query->execute($params);
            file_put_contents('warden.log', $query->queryString.$params[0]."\n", FILE_APPEND);
            //getting the objects from database
            $wardens = [];
            foreach ($query->fetchAll() as $warden){
                $warden->setId(         sha1($warden->getId().$this->table));
                $warden->setCategory(   $this->category);
                $warden->setHash(       'md5:'.$warden->getHash());
                $warden->setDetectTime( date('c', strtotime($warden->getDetectTime())));
                $wardens[] = $warden;
                Pool::dispose($warden);
            }
            return $wardens;
        }else{
            throw new Exception("Pass the parameter as array!", 42);
        }
    }

    /**
     * @return boolean The connection status
     */
    public function isConnected(){
        try{
            return (boolean) $this->pdo->query('SELECT 1');
        } catch (Exception $ex) {
            return false;
        }
    }
}
