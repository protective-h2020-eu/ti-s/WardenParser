<?php
namespace warden\src;

use warden\src\Warden;

/**
 * Pool of \warden\src\Warden objects
 * @author Jorgeley <jinacio@theemaillaundry.com>
 */
abstract class Pool implements \Countable{
    
    /**
     * @var array The busy \warden\src\Warden objects
     */
    private static $busyWardens = [];
    
    /**
     *
     * @var array The free \warden\src\Warden objects
     */
    private static $freeWardens = [];
    
    /**
     * @return \warden\src\Warden Get a Warden object
     */
    public function get(){
        if (count(self::$freeWardens) == 0){
            $warden = new Warden;
        }else{
            $warden = array_pop(self::$freeWardens);
        }
        self::$busyWardens[spl_object_hash($warden)] = $warden;
        return $warden;
    }
    
    /**
     * @param \warden\src\Warde $warden Warden object to put into the pool
     */
    public function set($warden){
        self::$busyWardens[spl_object_hash($warden)] = $warden;
    }
    
    /**
     * Dispose a \warden\src\Warden object
     * @param \warden\src\Warden $warden
     */
    public function dispose($warden){
        $key = spl_object_hash($warden);
        if (isset(self::$busyWardens[$key])){
            echo "disposing $key...\n";
            unset(self::$busyWardens[$key]);
            self::$freeWardens[$key] = $warden;
        }
    }
    
    /**
     * unset all the pool
     */
    public function disposeAll(){
        echo "disposing all...\n";
        self::$busyWardens = array();
        self::$freeWardens = array();
    }

    /**
     * @return int The \warden\src\Warden total objects in the pool
     */
    public function count() {
        return count(self::$busyWardens) + count(self::$freeWardens);
    }

}