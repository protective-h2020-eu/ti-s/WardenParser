<?php
namespace warden\src;
require_once __DIR__.'/IdeaCategory.php';
require_once __DIR__.'/Pool.php';
use warden\src\Pool;
use Exception;
use warden\src\IdeaCategory;

/**
 * Class that represents the 'idea' file
 * @access public
 * @author Jorgeley, <jinacio@theemaillaundry.com>
 */
class Warden implements \JsonSerializable{

    /**
     * @var Idea identifier
     * @access private
     */
    private $id;

    /**
     * @var Idea name
     * @access private
     */
    private $name = 'ideaFile';

    /**
     * @var Hash spam, malware, etc signature
     * @access private
     */
    private $hash;

    /**
     * @var The idea format, it seens that it has just this option
     * @access private
     */
    private $format = 'IDEA0';
    
    /**
     * @var Timestamp of detection in format: 1996-12-19T16:39:57-08:00
     * @access private
     * @see https://tools.ietf.org/html/rfc3339
     */
    private $detectTime;
    
    /**
     * @var array Constant idea category
     * @access private
     * @see \warden\src\IdeaCategory
     */
    private $category;
    
    /**
     * @var How many times the incident happened
     * @access private
     */
    private $count = 1;
    
    /**
     * @var Hexadecimal malware,virus, etc signature 
     */
    private $hex;
    
    /**
     * @var Confidence of the detection, 0 = false positive, default =1
     * @access private
     */
    private $active = 1;
    
    /**
     * Just in case if someone try to echo the object
     * @return json
     */
    public function __toString() {
        return json_encode($this->jsonSerialize());
    }
    
    /**
     * JSON idea file representation
     */
    public function jsonSerialize(){
        $jsonData = array(
                    "ID"            =>  $this->getId(),
                    "Format"        =>  $this->getFormat(),
                    "DetectTime"    =>  $this->getDetectTime(),
                    "Category"      =>  $this->getCategory(),
                    "Confidence"    =>  $this->getActive(),
                    //this structure [array()] is necessary for the idea pattern
                    "Attach"        =>  [array(
                                        "Hash"      =>  [ $this->getHash() ],
                                        "Content"   =>  $this->getHex(),
                                        "Filename"  =>  [ $this->getName() ],
                                    )],
                    "_count"        => $this->getCount()
        );
        //change null values by reference
        array_walk_recursive(
                $jsonData,
                function (&$value){
                    $value = ($value == null) ? 'no data' : $value;
                }
        );
        return $jsonData;
    }

    /**
     * @access public
     * @return The idea identifier
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @access public
     * @return The idea file name
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @access public
     * @return The Hash spam, malware, etc signature
     */
    public function getHash(){
        return $this->hash;
    }
    
    /**
     * @access public
     * @return The idea format
     */
    public function getFormat(){
        return $this->format;
    }
    
    /**
     * @access public
     * @return The time of detection
     */
    public function getDetectTime(){
        return $this->detectTime;
    }
    
    /**
     * @access public
     * @return array An array in format ['Category.Subcategory', 'Description']
     */
    public function getCategory(){
        return $this->category;
    }
    
    /**
     * @access public
     * @return How many times the incident happened
     */
    public function getCount(){
        return $this->count;
    }
    
    /**
     * @return Hexadecimal malware, virus, etc signature
     */
    public function getHex(){
        return $this->hex;
    }
    
    /**
     * @access public
     * @return The confidence of the detection, 0 = false positive, default = 1
     */
    public function getActive(){
        return $this->active;
    }

    /**
     * Set the idea identifier
     * @access public
     * @param  $id
     * @throws Exception
     */
    public function setId($id){
        if (preg_match("^[a-zA-Z0-9._-]+$^", $id) == 0){
            throw new Exception("The ID is not valid!\n May thus contain only "
                        . "alphanumeric, dot, minus sign and underscore", 30);
        }
        $this->id = $id;
    }

    /**
     * Set the idea name
     * @access public
     * @param  $name
     */
    public function setName($name='ideaFile'){
        $this->name = $name;
    }

    /**
     * Set the hash spam, malware, etc signature
     * @access public
     * @param  $hash
     * @throws Exception
     */
    public function setHash($hash){
        if (preg_match("^[a-zA-Z][a-zA-Z0-9+.-]*:[][a-zA-Z0-9._~:/?#@*'&'()*+,;=%-]*$^", $hash) == 0)
            throw new Exception ("This hash is invalid!\n"
                                . "Sample formats accepted:\n"
                                . "sha1:794467071687f7c59d033f4de5ece6b46415b633\n"
                                . "md5:dc89f0b4ff9bd3b061dd66bb66c991b1", 33);
            $this->hash = $hash;
    }
    
    /**
     * Set the idea format, it seens that will be allways "IDEA0"
     * @param $format
     */
    public function setFormat($format="IDEA0"){
        $this->format = $format;
    }
    
    /**
     * Set the idea detect time
     * @param $detectTime
     * @throws Exception
     */
    public function setDetectTime($detectTime){
        $pattern = "^[0-9]{4}-[0-9]{2}-[0-9]{2}[Tt ][0-9]{2}:[0-9]{2}:[0-9]{2}(?:\\.[0-9]+)?(?:[Zz]|(?:[+-][0-9]{2}:[0-9]{2}))?$^";
        if (preg_match($pattern, $detectTime) == 0)
            throw new Exception ("This is not a valid Timestamp!\n"
                                . "Sample format accepted: 1996-12-19T16:39:57-08:00", 32);
            $this->detectTime = date('c', strtotime($detectTime));
    }
    
    /**
     * Set the idea category
     * @see \warden\src\IdeaCategory
     * @param array $category
     * @throws Exception
     */
    public function setCategory($category){
        if ( !is_array($category) /** @todo Think about it || (!in_array($category, IdeaCategory::getConstantsAll()))*/ )
            throw new Exception ("This Idea Category is invalid!\n "
                                . "Use one of the IdeaCategory class constants:\n" 
                                .implode(', ', IdeaCategory::getConstantsNames()), 31);
        $this->category = [ $category[0],
                            /** @todo remove this when Cesnet switch us to production*/
                            IdeaCategory::TEST[0]
                          ];
    }
    
    /**
     * Set how many times this incident happened
     * @param $count
     */
    public function setCount($count){
        if (is_int($count)){
            $this->count = $count;
            //throw new Exception("This is not an integer number!", 33);
        }
    }
    
    /**
     * Set The confidence of the detection, 0 = false positive, default = 1
     * @param $active
     */
    public function setActive($active){
        if (is_bool($active)){
            $this->active = $active;
            //throw new Exception("This is not an integer number!", 33);
        }
    }
    
    /**
     * Set the hexadecimal virus, malware, etc signature
     * @param $hexadecimal
     */
    public function setHex($hexadecimal){
        $this->hex = $hexadecimal;
    }
}
