<?php
namespace warden\src;
require_once __DIR__.'/MysqlDAO.php';
use Exception;
use warden\src\MysqlDAO;

/**
 * Factory class responsible for instantiating the specific DAO's for 
 * each data source, eg: Mysql, SQL Server, etc
 * @abstract
 * @access public
 * @author Jorgeley, <jinacio@theemaillaundry.com>
 */
abstract class DAOFactory{

    /**
     * Constant for instantiating a Mysql database 'av_signatures', table 
     * 'custom_signatures' MysqlDAO object
     * @access public
     * @var int
     */
    const MYSQL_AVCUSTOMSIGNATURES = 1;

    /**
     * Constant for instantiating a Mysql database 'av_signatures', table 
     * 'url_signatures' MysqlDAO object
     * @access public
     * @var int
     */
    const MYSQL_AVURLSIGNATURES = 2;

    /**
     * Constant for instantiating a Mysql database 'hashdb', table 
     * 'emailhashes' MysqlDAO object
     * @access public
     * @var int
     */
    const MYSQL_EMAILHASHES = 3;

    /**
     * Constant for instantiating a Mysql database 'hashdb', table 
     * 'hashes' MysqlDAO object
     * @access public
     * @var int
     */
    const MYSQL_HASHES = 4;

    /**
     * Constant for instantiating a Mysql database 'spamdb', table 
     * 'hashes' MysqlDAO object
     * @access public
     * @var int
     */
    const MYSQL_SPAMHASHES = 5;

    /**
     * Returns a DAO's instance according to the $dao param
     * @access public
     * @param  int $dao The DAOFactory constant
     * @throws Exception
     */
    public function getDAO($dao){
        switch ($dao){
            case self::MYSQL_AVCUSTOMSIGNATURES:
                return new MysqlDAO(
                                    'av_signatures', //database
                                    'custom_signatures', //table
                                    IdeaCategory::MALWARE_GENERAL, //category
                                    [// Table_field     =>  WardenClassAttribute
                                        'id'            =>  'id',
                                        'mail_id'       =>  'name',
                                        'md5'           =>  'hash',
                                        'hex'           =>  'hex',
                                        'count'         =>  'count',
                                        'active'        =>  'active',
                                        'date_modified' =>  'detectTime'
                                    ]
                            );
                break;
            case self::MYSQL_AVURLSIGNATURES:
                return new MysqlDAO(
                                    'av_signatures', //database
                                    'url_signatures', //table
                                    IdeaCategory::MALWARE_GENERAL, //category
                                    [// Table_field     =>  WardenClassAttribute
                                        'id'            =>  'id',
                                        'item_id'       =>  'name',
                                        'md5'           =>  'hash',
                                        'hex'           =>  'hex',
                                        'count'         =>  'count',
                                        'active'        =>  'active',
                                        'date_modified' =>  'detectTime'
                                    ]
                            );
                break;
            case self::MYSQL_EMAILHASHES:
                return new MysqlDAO(
                                    'hashdb', //database
                                    'emailhashes', //table
                                    IdeaCategory::SPAM_SENDING_ADDRESS, //category
                                    [// Table_field   =>  WardenClassAttribute
                                        'id'          =>  'id',
                                        //'email'       =>  'name',
                                        'hash'        =>  'hash',
                                        'count'       =>  'count',
                                        'last_update' =>  'detectTime'
                                    ]
                            );
                break;
            /** Not parsing this for now
            case self::MYSQL_HASHES:
                return new MysqlDAO(
                                    'hashdb', //database
                                    'hashes', //table
                                    [// Table_field   =>  WardenClassAttribute
                                        'id'          =>  'id',
                                        'hash'        =>  'hash',
                                        'count'       =>  'count',
                                        'last_update' =>  'detectTime'
                                    ]
                            );
                break;
            case self::MYSQL_SPAMHASHES:
                return new MysqlDAO(
                                    'spamdb', //database
                                    'hashes', //table
                                    [// Table_field   =>  WardenClassAttribute
                                        'id'          =>  'id',
                                        'hash'        =>  'hash',
                                        'count'       =>  'count',
                                        'last_update' =>  'detectTime'
                                    ]
                            );
                break;
             */
            /**
             * @todo new DAO's coming soon
             */
            default:
                throw new Exception('This DAO is not implemented!!!');
        }
    }

}
