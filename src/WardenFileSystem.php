<?php
namespace warden\src;
require_once __DIR__.'/Pool.php';
use warden\src\Pool;
use Exception;

/**
 * Class responsible for manipulating JSON idea files
 * @author Jorgeley <jinacio@theemaillaundry.com>
 */
class WardenFileSystem{

    /**
     * @var Directory to export de idea files
     * @access private
     */
    private $exportDir = __DIR__ . '/../idea/';

    /**
     * @var Directory for exporting old idea files
     * @access private
     */
    const EXPORT_DIR_INCOMING = __DIR__ . '/../idea/incoming/';    

    /**
     * Returns an array of Warden objects instantiated from export $exportDir
     * @access public
     * @return array An array of Json's idea, eg: 
     *                  ['filename1'] => [
     *                                      ["id"]  => "123abc",
     *                                      ["name"]=> "idea name",
     *                                      ...
     *                                  ],
     *                  ['filename2] => ...
     * @throws Exception
     */
    public function getIdeasFromFileSystem(){
        echo "\n reading directory '". $this->getDir() ."'...\n";
        $d = $this->getDirResource();
        $ideasArray = [];
        while (false !== ($ideaFile = readdir($d))){
            if (!is_dir($this->exportDir.$ideaFile) && $ideaFile != "." && $ideaFile != ".."){
                $filename = $this->exportDir.$ideaFile;
                $f = @fopen($filename, 'r');
                $ideasArray[ $ideaFile ] = 
                                    json_decode(
                                            fread($f, 
                                                  filesize($filename)
                                            ), 
                                    true);
            }
        }
        //print_r($ideasArray);
        closedir($d);
        return $ideasArray;
    }

    /**
     * Export Warden objects to filesystem
     * @param array $wardens Array Warden objects to be exported to filesystem
     */
    public function export($wardens){
        foreach ($wardens as $warden){
            $filename = preg_replace("/\W/", "",
                            password_hash(
                                $warden->getId().
                                $warden->getName()
                            , PASSWORD_BCRYPT)
                        )
                        .'.'.$warden->getCount()
                        .'.idea';            
            echo " exporting '".$this->exportDir."$filename'...\n";
            file_put_contents('warden.log', "exporting '".$this->exportDir."$filename'...\n", FILE_APPEND);
            $fResource = fopen($this->exportDir.$filename, 'w+');
            fwrite($fResource, $warden->__toString());
            fclose($fResource);
            copy($this->exportDir.$filename, self::EXPORT_DIR_INCOMING.$filename);
            Pool::dispose($warden);
	    file_put_contents(__DIR__.'/../warden.log', "[".date('d/m/y H:i:s')."] exporting '".$this->exportDir."$filename'...\n", FILE_APPEND);
        }
    }

    /**
     * @return The directory for exporting idea files
     */
    public function getDir(){
        return $this->exportDir;
    }
    
    /**
     * @param $dir The directory for exporting the idea files
     */
    public function setDir($dir){
        $this->exportDir = $dir;
    }
    
    /**
     * @return resource The directory resource handle
     * @throws Exception
     */
    private function getDirResource(){
        if ( ($d = @opendir($this->exportDir)) === FALSE ){
            throw new Exception("Error trying to read dir ".$this->exportDir, 20);
        }
        return $d;
    }
    
}
