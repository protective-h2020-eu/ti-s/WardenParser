<?php
namespace warden\src;

/**
 * Interface for DAO's
 * @access public
 * @author Jorgeley, <jinacio@theemaillaundry.com>
 */
interface DAO{

    /**
     * It might garantee that find unique Idea objects from the Data Source
     * @param  array The wardens ID already found and exported to the filesystem
     */
    public function findByUniqueWardens($wardensIds);
    
    /**
     * Find new Idea files since last execution specified by $time, eg: 
     *              00:30:00 (it fetch new ideas from 30 minutes ago)
     * @param $time 
     */
    public function findByDetectTime($time);
    
    /**
     * It might garantee that the connection is ok
     */
    public function isConnected();

}