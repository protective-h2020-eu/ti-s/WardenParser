<?php
namespace warden\src;
require_once __DIR__.'/Pool.php';
require_once __DIR__.'/Warden.php';
require_once __DIR__.'/WardenFileSystem.php';
use warden\src\Pool;
use warden\src\WardenFileSystem;
use DeepCopy\Exception\CloneException;

/**
 * Class responsible for manipulating Warden objects: importing, exporting, etc
 * @access public
 * @author Jorgeley, <jinacio@theemaillaundry.com>
 */
class WardenManager extends WardenFileSystem{
    
    /**
     * @var \warden\src\WardenManagaer The WardenManager instance
     */
    private static $wardenManager;

    /**
     * @var Constant Warden version
     */
    const WARDEN_VERSION = '';
    
    /**
     * @var array Array of Warden objects
     */
    private $wardens = [];
    
    /**
     * no one can access the constructor
     */
    private function __construct(){        
    }
    
    /**
     * not even clone
     * @throws CloneException
     */
    public function __clone(){
        throw new CloneException();
    }
    
    /**
     * Get an instance of WardenManager
     * @return \warden\src\WardenManager
     */
    public static function getWardenManager(){
        if (self::$wardenManager == null){
            self::$wardenManager = new WardenManager;
        }
        return self::$wardenManager;
    }
    
    /**
     * Instantiate Warden objects for each Json idea from $ideasArray and
     * store them into self::wardens array
     * @param array $ideasArray An array of Json idea contents and name as key, eg:
     *                            ['filename1'] => [
     *                                                  ["id"]  => "123abc",
     *                                                  ["name"]=> "idea name",
     *                                                   ...
     *                                              ],
     *                            ['filename2] => ...
     * @todo I'd better make a pool here to save memory
     */
    public function import($ideasArray){
        echo "\n importing idea objects...\n";
        if (is_array($ideasArray)){
            foreach ($ideasArray as $fileName=>$ideaArray){
                try{
                    $warden = Pool::get();
                    $warden->setId(         $ideaArray['ID'] );
                    $warden->setName(       $ideaArray['Attach'][0]['Filename'][0]);
                    $warden->setHash(       $ideaArray['Attach'][0]['Hash'][0]);
                    $warden->setHex(        $ideaArray['Attach'][0]['Content']);
                    $warden->setFormat(     $ideaArray['Format']);
                    $warden->setDetectTime( $ideaArray['DetectTime']);
                    $warden->setCategory(   $ideaArray['Category']);
                    $warden->setActive(     $ideaArray['Confidence']);
                    $warden->setCount(      $ideaArray['_count']);
                    $this->wardens[$fileName] = $warden;
                    Pool::dispose($warden);
                } catch (Exception $e){
                    echo "Error trying to set warden from filesystem!";
                }
            }
        }
        //print_r($this->wardens);
    }
    
    /**
     * @return array An array of wardens Id's
     */
    public function getWardensIDs(){
        $wardensIDs = [];
        foreach ($this->wardens as $warden){
            $wardensIDs[] = $warden->getId();
            Pool::dispose($warden);
        }
        return $wardensIDs;
    }
    
    /**
     * @return array Array of \warden\src\Warden objects
     */
    public function getWardens(){
        return $this->wardens;
    }
    
    /**
     * @param array $wardens Array of \warden\src\Warden objects
     */
    public function setWardens($wardens){
        $this->wardens = $wardens;
    }
    
    public function unsetWardens(){
        foreach ($this->wardens as $key=>$value)
            unset ($this->wardens[$key]);
    }
}