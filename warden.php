<?php
/** 
 * Main Warden Manager PHP script
 * @author Jorgeley <jinacio@theemaillaundry.com>
 */
require_once __DIR__.'/src/DAOFactory.php';
require_once __DIR__.'/src/WardenManager.php';
require_once __DIR__.'/src/Pool.php';
use warden\src\Pool;
use warden\src\DAOFactory;
use warden\src\WardenManager;

echo "\n *** The Email Laundry Warden Manager ***\n";

if (isset($_SERVER['HTTP_USER_AGENT'])){
    echo "<p>Please, execute this script from the command line, like:<br>"
                        . "<i>php warden.php</i></p>";
    exit;
}else{
    //find wardens updated 1 minutes ago
    $time = '00:01:00';
    file_put_contents('warden.log', "\n[".date('d/m/y H:i:s')."] Starting warden parser...\n", FILE_APPEND);  
    file_put_contents('warden.log', "Checking for new ideas files every $time...\n", FILE_APPEND);
    
    //instantiate the WardenManager responsible for manipulating the wardens
    $wardenManager = WardenManager::getWardenManager();
    
    //get Mysql DAO instance for 'av_signatures' database, table 'custom_signatures'
    $avCustomSignaturesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_AVCUSTOMSIGNATURES );
    
    //get Mysql DAO instance for 'av_signatures' database, table 'url_signatures'
    $avUrlSignaturesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_AVURLSIGNATURES );
    
    //get Mysql DAO instance for 'hashdb' database, table 'emailhashes'
    $emailHashesMysqlDAO = DAOFactory::getDAO( DAOFactory::MYSQL_EMAILHASHES );

    while (true){
       try{
           file_put_contents('warden.log', Pool::count()." objects in memory...\n", FILE_APPEND);

           //find the latest database 'av_signatures', table 'custom_signatures'
           $ideasUpdated = $avCustomSignaturesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
        
           //find the latest database 'av_signatures', table 'url_signatures'
           $ideasUpdated = $avUrlSignaturesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
        
           //find the latest database 'hashdb', table 'emailhashes'
           $ideasUpdated = $emailHashesMysqlDAO->findByDetectTime($time);
           $wardenManager->export($ideasUpdated);
           foreach ($ideasUpdated as $key=>$value) unset($ideasUpdated[$key]);
       }catch(Exception $e){
           echo $e->getMessage();
           file_put_contents('warden.log', '['.date('d/m/y H:i:s').'] ERROR: '.$e->getMessage()."\n", FILE_APPEND);
       }
       sleep(strtotime("01/01/1970 $time") + 3600);
    }
}
