<?php
namespace warden\tests\src;
require_once __DIR__.'/../../src/Warden.php';
require_once __DIR__.'/../../src/IdeaCategory.php';
use Exception;
use PHPUnit\Framework\TestCase;
use warden\src\Warden;
use warden\src\IdeaCategory;

/**
 * Unit test from \warden\src\Warden
 */
class WardenTest extends TestCase{

    /**
     * @var Warden The \warden\src\Warden object
     */
    protected $warden;

    /**
     * Instantiate a Warden and set attributes
     */
    protected function setUp(){
        $sha1 = 'sha1:794467071687f7c59d033f4de5ece6b46415b633';
        $this->warden = new Warden;
        $this->warden->setId(           '123wardenId456');
        $this->warden->setName(         'wardenTest');
        $this->warden->setHash(         $sha1);
        $this->warden->setHex(          'hex123hex');
        //                              date        hour    timezone
        $this->warden->setDetectTime(   '1996-12-19 16:39:57');
        $this->warden->setCategory(     IdeaCategory::MALWARE_GENERAL);
        $this->warden->setCount(        1);
    }

    /**
     * Destroy the Warden object
     */
    protected function tearDown(){
        $this->warden = null;
    }
    
    public function testWardenIsInstantiated(){
        $this->assertInstanceOf('warden\src\Warden', $this->warden);
    }
    
    /**
     * @covers \warden\src\Warden::__toString()
     */
    public function testWardenEchoJson(){
        $this->assertJson(sprintf($this->warden));
    }

    /**
     * @covers \warden\src\Warden::getId()
     */
    public function testGetId(){
        $this->assertEquals('123wardenId456', $this->warden->getId());
    }

    /**
     * @covers \warden\src\Warden::getName()
     */
    public function testGetName(){
        $this->assertEquals('wardenTest', $this->warden->getName());
    }

    /**
     * @covers \warden\src\Warden::getHash()
     */
    public function testGetHash(){
        $sha1 = 'sha1:794467071687f7c59d033f4de5ece6b46415b633';
        $this->assertEquals($sha1, $this->warden->getHash());
    }
    
    /**
     * @covers \warden\src\Warden::getDetectTime()
     */
    public function testGetDetectTime(){
        $this->assertEquals('1996-12-19T16:39:57+00:00', $this->warden->getDetectTime());
    }
    
    /**
     * @covers \warden\src\Warden::getCategory()
     */
    public function testGetCategory(){
        $this->assertEquals([   
                                IdeaCategory::MALWARE_GENERAL[0],
                                IdeaCategory::TEST[0]
                            ],
                            $this->warden->getCategory()
                        );
    }
    
    /**
     * @covers \warden\src\Warden::getCount()
     */
    public function testGetCount(){
        $this->assertEquals(1, $this->warden->getCount());
    }
    
    /**
     * @covers \warden\src\Warden::getHex()
     */
    public function testGetHex(){
        $this->assertEquals('hex123hex', $this->warden->getHex());
    }

    /**
     * @covers \warden\src\Warden::setId($id)
     */
    public function testSetId(){
        $this->warden->setId('789wardenId012');
        $this->assertEquals('789wardenId012', $this->warden->getId());
    }
    
    /**
     * Test id against pattern needed
     * @covers \warden\src\Warden::setId($id)
     * @expectedException Exception
     * @expectedExceptionCode 30
     */
    public function testInvalidId(){
        $this->warden->setId("#invalid ID#");
    }

    /**
     * @covers \warden\src\Warden::setName($name)
     */
    public function testSetName(){
        $this->warden->setName('wardenTest2');
        $this->assertEquals('wardenTest2', $this->warden->getName());
    }

    /**
     * @covers \warden\src\Warden::setHash($hash)
     */
    public function testSetHash(){
        $this->warden->setHash('sha1:794467071687f7c59d033f4de5ece6b46415b634');
        $this->assertEquals('sha1:794467071687f7c59d033f4de5ece6b46415b634', $this->warden->getHash());
    }
    
    /**
     * @covers \warden\src\Warden::setHash($hash)
     * @expectedException Exception
     * @expectedExceptionCode 33
     */
    public function testSetHashInvalid(){
        $this->warden->setHash('invalid hash');
    }
    
    /**
     * @covers \warden\src\Warden::setDetectTime($detectTime)
     */
    public function testSetDetectTime(){
        $this->warden->setDetectTime('1996-12-19 16:39:57');
        $this->assertEquals('1996-12-19T16:39:57+00:00', $this->warden->getDetectTime());
    }
    
    /**
     * @covers \warden\src\Warden::setDetectTime($detectTime)
     * @expectedException Exception
     * @expectedExceptionCode 32
     */
    public function testSetDetectTimeInvalid(){
        $this->warden->setDetectTime('1996/12/19U16.39.57\03:00 INVALID');
    }    

    /**
     * @covers \warden\src\Warden::setCategory($category)
     */
    public function testSetCategory(){
        $this->warden->setCategory(IdeaCategory::MALWARE_GENERAL);
        $this->assertEquals([   
                                IdeaCategory::MALWARE_GENERAL[0],
                                IdeaCategory::TEST[0]
                            ],
                            $this->warden->getCategory()
                        );
    }
    
    /**
     * @covers \warden\src\Warden::setCategory($category)
     * @expectedException Exception
     * @expectedExceptionCode 31
     */
    public function testSetCategoryInvalid(){
        $this->warden->setCategory('CATEGORY_INVALID');
    }
    
    /**
     * @covers \warden\src\Warden::setCount($count)
     */
    public function testSetCount(){
        $this->warden->setCount(2);
        $this->assertEquals(2, $this->warden->getCount());
    }
    
    /**
     * @covers \warden\src\Warden::setCount($count)
     * @expectedException Exception
     * @expectedExceptionCode 33
     */
    public function testSetCountInvalid(){
        $this->markTestSkipped();
        $this->warden->setCount('x');
    }
    
    /**
     * @covers \warden\src\Warden::setHex($hexadecimal)
     */
    public function testSetHex(){
        $this->warden->setHex('hex456hex');
        $this->assertEquals('hex456hex', $this->warden->getHex());
    }
            
}