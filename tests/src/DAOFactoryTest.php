<?php
namespace warden\tests\src;
require_once __DIR__.'/../../src/DAOFactory.php';
require_once __DIR__.'/../../src/MysqlDAO.php';
use \PHPUnit\Framework\TestCase;
use warden\src\DAOFactory;
use warden\src\MysqlDAO;

/**
 * Unit Test for \warden\src\DAOFactory
 */
class DAOFactoryTest extends TestCase{

    /**
     * @var DAOFactory The Mysql DAO Factory object
     */
    protected $mysqlDAO;

    /**
     * Get a Mysql DAO instance from the Factory
     */
    protected function setUp(){
        $this->mysqlDAO = DAOFactory::getDAO(DAOFactory::MYSQL_AVCUSTOMSIGNATURES);
    }

    /**
     * Destroy the Mysql DAO object
     */
    protected function tearDown(){
        $this->mysqlDAO = null;
    }
    
    /**
     * Garantee that not implemented DAOS aren't accepted
     * @covers \warden\src\DAOFactory::getDAO($dao)
     * @expectedException Exception
     */
    public function testNotAcceptUnimplementedDAOS(){
        DAOFactory::getDAO(-1);
    }

    /**
     * @covers \warden\src\DAOFactory::getDAO($dao)
     */
    public function testGetDAO(){
        $this->assertInstanceOf('warden\src\MysqlDAO', $this->mysqlDAO);
    }

}